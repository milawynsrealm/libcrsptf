# Simple, Cross-Platform Multimedia Library #

A simple cross-platform library mostly for multimedia applications. This library is designed to allow easier development even when you are not familiar with specific platforms.

## Note for Windows Developers ##

This library is currently designed to be able to run comfortably in Windows XP or later. Although this version is now considered to be deprecated by most people, the open source development of [ReactOS](http://reactos.org/) has, for the moment, kept that support still alive. However, if the kernel target of ReactOS should switch to a later version of Windows (e.g. Windows 7), the target may also change.

## Building ##

Compiling this library should be easy since all you have to do is just drop the headers into your project and compile. However, if you want to compile in Windows under GCC, then [Mingw-w64](https://mingw-w64.org/) is recommended as it provides better support than [MinGW](https://osdn.net/projects/mingw/). It should also be able to be built under MinGW, but this hasn't yet been tested yet.

### Windows Dependencies ###

* shell32
* advapi32
* kernel32

## Disclaimer ##
Yes, this is the continuation of the [gamedev](https://github.com/milawynsrealm/gamedev) library, and yes, it has been put under a different license. The code over here is where my focus is going to be for now. I will leave the other codebase up mostly for reference purposes.

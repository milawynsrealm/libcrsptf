/*
 * Project: Simple Cross-Platform Multimedia Library
 * License: MIT
 * Author: Lee Schroeder <spaceseel at gmail dot com>
 */
#ifndef _CRSPTF_H
#define _CRSPTF_H

#ifdef __cplusplus
extern "C" {
#endif

/* Be sure to include this shared header file if
 * you are including everything separately */
#include "crsptf/crsptf_shared.h"

#include "crsptf/crsptf_main.h"
#include "crsptf/crsptf_system.h"
#include "crsptf/crsptf_log.h"
#include "crsptf/crsptf_path.h"

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* _CRSPTF_H */

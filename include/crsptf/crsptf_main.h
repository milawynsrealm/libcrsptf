/*
 * Project: Simple Cross-Platform Multimedia Library
 * License: MIT
 * Author: Lee Schroeder <spaceseel at gmail dot com>
 */
#ifndef _CRSPTF_MAIN_H
#define _CRSPTF_MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _CRSPTF_SHARED_H
#error crsptf_shared.h is needed to compile.
#endif

#if (CURRENT_OS == OSNAME_WINDOWS)
#include <windef.h>
#include <winbase.h>
#endif

/* This function is defined by the user inside of the application itself. If the compiler
   cannot find this function, then make sure the function exists in the source code and
   everything is linked properly. */
extern int AppMain(int argc, char *argv[]);

#if (CURRENT_OS == OSNAME_WINDOWS) && !defined(USE_CONSOLE_MODE)
/* Although Windows now uses the wWinMain() function for modern Win32 applications, we
   use the ANSI version here to keep things simple. If the developer wants to call the
   UNICODE arguments, then it is recommended for the developer to use the CommandLineToArgvW()
   function instead. */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pCmdLine, int nCmdShow)
{
    return AppMain(__argc, __argv);
}
#else /* POSIX-based */
int main(int argc, char *argv[])
{
    return AppMain(argc, argv);
}
#endif /* _WIN32 */

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* _CRSPTF_MAIN_H */

/*
 * Project: Simple Cross-Platform Multimedia Library
 * License: MIT
 * Author: Lee Schroeder <spaceseel at gmail dot com>
 */
#ifndef _CRSPTF_SHARED_H
#define _CRSPTF_SHARED_H

#ifdef __cplusplus
extern "C" {
#endif

/* Used to identify the OS type */
#define OSNAME_NONE    -1
#define OSNAME_WINDOWS  0
#define OSNAME_LINUX    1
#define OSNAME_BSD      2
#define OSNAME_MACOS    3
#define OSNAME_ANDROID  4
#define OSNAME_BEOS     5
#define OSNAME_OS2      6
#define OSNAME_HURD     7

/* Used to make reading source code used to determine the
   OS type when compiling easy */
#if defined(__linux__) || defined(__gnu_linux__)
#define CURRENT_OS OSNAME_LINUX /* GNU/Linux */
#elif defined(__GNU__) && defined(__gnu_hurd__)
#define CURRENT_OS OSNAME_HURD /* GNU/Hurd */
#elif defined(__FreeBSD__) || defined(__OpenBSD__) || \
      defined(__NetBSD__) || defined(__bsdi__) || \
      defined(__DragonFly__)
#define CURRENT_OS OSNAME_BSD /* BSD-based */
#elif defined(__APPLE__) && defined(__MACH__)
#define CURRENT_OS OSNAME_MACOS /* macOS */
#elif defined(OS2) || defined(_OS2)
#define CURRENT_OS OSNAME_OS2 /* OS/2 */
#elif defined(_WIN32)
#define CURRENT_OS OSNAME_WINDOWS /* Windows NT */
#elif defined(__ANDROID__)
#define CURRENT_OS OSNAME_ANDROID /* Android */
#elif defined(__BEOS__)
#define CURRENT_OS OSNAME_BEOS /* BeOS */
#else
#define CURRENT_OS OSNAME_NONE /* Unknown */
#endif

/* Used internally to detect if a system is POSIX-based since
   Linux, BSD and macOS shares a similar core UNIX-based
   architecture. Not perfect, but it simplifies a lot of
   work and keeps the code cleaner. */
#if (CURRENT_OS == OSNAME_LINUX) ||\
      (CURRENT_OS == OSNAME_BSD) ||\
      (CURRENT_OS == OSNAME_MACOS)
#define IS_POSIX
#endif

/* Used to determine the Architecture type */
#define ARCH_NONE    -1
#define ARCH_86      0
#define ARCH_AMD64   1
#define ARCH_ITANIUM 2
#define ARCH_ARM     3
#define ARCH_ALPHA   4
#define ARCH_PPC     5

/* Used to derermine the minimum version of Windows */
#define MINOS_NONE  0
#define MINOS_XP    1
#define MINOS_VISTA 2
#define MINOS_7     3
#define MINOS_8     4

/* Determines the current system architecture */
#if defined(__i386) || defined(_M_IX86)
#define CURRENT_ARCH ARCH_86;
#elif defined(__amd64__) || defined(_M_X64)
#define CURRENT_ARCH ARCH_AMD64;
#elif defined(__ia64__) || defined(_M_IA64)
#define CURRENT_ARCH ARCH_ITANIUM;
#elif defined(__arm__) || defined(_M_ARM)
#define CURRENT_ARCH ARCH_ARM;
#elif defined(__alpha__) || defined(_M_ALPHA)
#define CURRENT_ARCH ARCH_ALPHA;
#elif defined(__powerpc) || defined(_M_PPC)
#define CURRENT_ARCH ARCH_PPC;
#else
#define CURRENT_ARCH ARCH_NONE;
#endif /* __i386 */

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* _CRSPTF_SHARED_H */

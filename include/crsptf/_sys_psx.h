#ifndef _SYS_PSX_H
#define _SYS_PSX_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <sys/utsname.h>

#define OS_LENGTH 256

int GetOsName_posix(char *name)
{
    struct utsname unameData;
    char tmpName[OS_LENGTH];

    /* Try to grab the name of the operating system the computer
       the user is currently using. */
    if (uname(&unameData) == 0)
    {
        /* Grabs the name of the operating system and version
           so it can better help with debugging. */
        /* Linux x.x.xx (xx) */
        sprintf(tmpName, "%s %s (%s)", unameData.sysname, unameData.release, unameData.version);
    }
    else
    {
        /* If uname doesn't work, then at least try to get the
           name of the operating system. */
#if (CURRENT_OS == OSNAME_LINUX)
        strcpy(tmpName, "GNU/Linux");
#elif (CURRENT_OS == OSNAME_HURD)
        strcpy(tmpName, "GNU/Hurd");
#elif (CURRENT_OS == OSNAME_BSD)
        strcpy(tmpName, ("BSD-based");
#elif (CURRENT_OS == OSTYPE_OSX)
        strcpy(tmpName, "macOS");
#else
        strcpy(tmpName, "POSIX-based OS");
#endif
    }

    /* Copy the name of the operating system */
    name = (char*)tmpName;

    /* Return the operating system type */
#if (CURRENT_OS == OSNAME_LINUX)
    return OSNAME_LINUX;
#elif (CURRENT_OS == OSNAME_BSD)
    return OSNAME_BSD;
#elif (CURRENT_OS == OSNAME_MACOS)
    return OSNAME_MACOS;
#elif (CURRENT_OS == OSNAME_HURD)
    return OSNAME_HURD;
#endif
    return OSNAME_NONE;
}

int GetProgramName_posix(char *name)
{
#if (CURRENT_OS == OSNAME_BSD) || \
    (CURRENT_OS == OSNAME_MACOS)
    /* Both BSD and OSX seem to use this method */
    strcpy(name, getprogname());
    return ((name == NULL) ? 1 : 0);
#elif (CURRENT_OS == OSNAME_LINUX)
    /* Linux seems to prefer this method */
    extern char *program_invocation_short_name;
    return ((strcpy(name, program_invocation_short_name) == NULL) ? 1 : 0);
#endif

    /* Shouldn't get here, but the compiler complains if we leave it out */
    return 1;
}

#endif /* _SYS_PSX_H */

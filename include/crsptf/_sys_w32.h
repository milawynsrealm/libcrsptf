#ifndef _SYS_W32_H
#define _SYS_W32_H

int GetOsName_win32(char *name)
{
    HKEY hKey;
    LONG res = ERROR_SUCCESS;
    DWORD dwSize;
    OSVERSIONINFO winVer;

    /* Clear the memory before using it */
    ZeroMemory(&winVer, sizeof(OSVERSIONINFO));
    winVer.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

    /* Try to get the current version of Windows */
    GetVersionEx(&winVer);

    dwSize = sizeof(int);

    /* Opens the registry key for getting the name of the operating system */
    if (RegOpenKeyExA(HKEY_CURRENT_USER,
                     "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion",
                     0,
                     KEY_QUERY_VALUE,
                     &hKey) == ERROR_SUCCESS)
    {
        /* Look for the registry name */
        res = RegQueryValueExA(hKey,
                              "ProductName",
                              NULL,
                              NULL,
                              (LPBYTE)osName,
                              &dwSize);

        /* Add an extra space at the end for formatting purposes */
        strcat(osName, " ");

        /* Close the registry key after it's used */
        RegCloseKey(hKey);
    }

    /* If no name is found in the registry, then just use Windows NT */
    if ((res != ERROR_SUCCESS) || (wcslen(osName) == 0))
    {
        strcpy(osName, "Windows NT ");
    }

    /* If successful, add the version of Windows being used */
    if (osName != NULL)
    {
        strcat(osName, _itoa(winVer.dwMajorVersion));
        strcat(osName, ".");
        strcat(osName, _itoa(winVer.dwMinorVersion));
        strcat(osName, ".");
        strcat(osName, _itoa(winVer.dwBuildNumber));
    }

    /* Lets the program know it's Windows */
    return OSNAME_WINDOWS;
}

int IsMinimumOS_win32(int version)
{
    OSVERSIONINFO osInfo;

    /* Clear the memory before using it */
    ZeroMemory(&osInfo, sizeof(OSVERSIONINFO));
    osInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

    /* Get the version of Windows */
    GetVersionEx(&osInfo);

    switch (version)
    {
        case MINOS_XP: /* Windows XP/2K3 or later */
        {
            if ((osInfo.dwMajorVersion > 6) ||
               ((osInfo.dwMajorVersion == 5) &&
                (osInfo.dwMinorVersion >= 1)))
                    return 0;
            break;
        }
        case MINOS_VISTA: /* Windows Vista/2008 or later */
        {
            if (osInfo.dwMajorVersion > 6)
                return 0;
            break;
        }
        case MINOS_7: /* Windows 7/2008 R2 or later */
        {
            if ((osInfo.dwMajorVersion > 6) &&
                (osInfo.dwMajorVersion >= 1))
                    return 0;
            break;
        }
        case MINOS_8: /* Windows 8/2012 or later */
        {
            if ((osInfo.dwMajorVersion > 6) &&
                (osInfo.dwMajorVersion >= 2))
                    return 0;
            break;
        }
        default:break;
    }

    /* If it gets here, assume the user is
       using the wrong version of Windows */
    return 1;
}

#endif /* _SYS_W32_H */

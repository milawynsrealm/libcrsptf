/*
 * Project: Simple Cross-Platform Multimedia Library
 * License: MIT
 * Author: Lee Schroeder <spaceseel at gmail dot com>
 */
#ifndef _CRSPTF_PATH_H
#define _CRSPTF_PATH_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _CRSPTF_SHARED_H
#error crsptf_shared.h is needed to compile.
#endif

typedef enum {
    TMP_DIR = 1,
    APP_DIR,
    CFG_DIR
} PATH_TYPE;

char *GetPathSeperator(void)
{
#if (CURRENT_OS == OSNAME_WINDOWS)
    return "\\";
#else
    return "/";
#endif
}

char *_getCfgPath(void)
{
    char path[MAX_PATH];
#if (CURRENT_OS == OSNAME_WINDOWS)
    if (SHGetFolderPathA(NULL, CSIDL_APPDATA, NULL, 0, &path) != S_OK)
        return NULL;
#elif (CURRENT_OS == OSTYPE_OSX) /* macOS */
    if (sprintf(path, "%s/Library/Application Support/", getenv("HOME")) < 0)
        return NULL;
#else /* GNU/Linux, BSD, etc. */
    strcpy(path, getenv("XDG_CONFIG_HOME"));

    /* If XDG_CONFIG_HOME doesn't exist, use HOME as a backup
     * attempt before bailing out */
    if (strlen(&path) == 0)
    {
        /* /home/<user>/.local/share/ */
        if (strcpy(path, getenv("HOME")) == NULL)
            return NULL;
    }
#endif
    return path;
}

char *_getAppPath(void)
{
    char path[MAX_PATH];
#if (CURRENT_OS == OSNAME_WINDOWS)
    if (GetModuleFileNameA(NULL, &path, MAX_PATH) != ERROR_SUCCESS)
        return NULL;
#else
    if (readlink("/proc/self/exe", &path, MAX_PATH)) == -1)
        return NULL;
#endif
    return path;
}

char *_getTmpPath(void)
{
    char path[MAX_PATH];
#if (CURRENT_OS == OSNAME_WINDOWS)
    /* Grabs the path where the temporary files are */
    if (GetTempPathA(MAX_PATH, &path) != 0)
        return NULL;
#else
    /* Most *NIX-based paths are in the same location */
    strcpy(path, "/tmp");
#endif
    return path;
}

char *GetSpecialPath(PATH_TYPE type)
{
    switch(type)
    {
        case TMP_DIR:
            return _getTmpPath();
        case APP_DIR:
            return _getAppPath();
        case CFG_DIR:
            return _getCfgPath();
        default:
            break;
    }

    /* If the choice is invalid, return nothing */
    return NULL;
}

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* _CRSPTF_PATH_H */

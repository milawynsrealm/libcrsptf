/*
 * Project: Simple Cross-Platform Multimedia Library
 * License: MIT
 * Author: Lee Schroeder <spaceseel at gmail dot com>
 */
#ifndef _CRSPTF_LOG_H
#define _CRSPTF_LOG_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>
#include <time.h>

#define LOG_ERROR_NONE    0
#define LOG_ERROR_NO_FILE 1

#define LOG_INFO  0
#define LOG_WARN  1
#define LOG_ERROR 2
#define LOG_FATAL 3

int _logSetDate(FILE *path)
{
    time_t currTime = time(NULL);
    struct tm tm = *localtime(&currTime);

    /* Make sure there's a file to work with */
    if (path == NULL)
        return LOG_ERROR_NO_FILE;

    /* Add the Timestamp to the log */
    fprintf(path, "%04d-%02d-%02d %02d:%02d:%02d", (tm.tm_year+1900), (tm.tm_mon+1), tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

    /* Return success */
    return LOG_ERROR_NONE;
}

int LogInit(FILE *path)
{
    /* Make sure there's a file to work with */
    if (path == NULL)
        return LOG_ERROR_NO_FILE;

    /* Put in the closing Time/Date */
    fputs("========== START LOG (", path);
    _logSetDate(path);
    fputs(") ==========\n", path);

    /* Return success */
    return LOG_ERROR_NONE;
}

int LogPrint(FILE *path, int type, char *message, ...)
{
    va_list args;

    /* Make sure there's a file to work with */
    if (path == NULL)
        return LOG_ERROR_NO_FILE;

    /* Add a header depending on what log type is called */
    switch(type)
    {
        /* Warning Message */
        case LOG_WARN:
        {
            _logSetDate(path);
            fputs(" - ", path);
            fputs("WARNING: ", path);
            break;
        }
        /* Error Message */
        case LOG_ERROR:
        {
            _logSetDate(path);
            fputs(" - ", path);
            fputs("ERROR: ", path);

            break;
        }
        /* Fatal Message */
        /* NOTE: It is not up to this function to exit the program
         * abruptly as each program uses it's own strategy to abort. */
        case LOG_FATAL:
        {
            _logSetDate(path);
            fputs(" - ", path);
            fputs("FATAL: ", path);
            break;
        }
        /* Information Message */
        default:
        {
            _logSetDate(path);
            fputs(" - ", path);
            break;
        }
    }

    /* Beginning of message loop */
    va_start(args, message);

    /* Get each section of the message together */
    vfprintf(path, message, args);

    /* The end of the message loop */
    va_end(args);

    /* Make sure each entry is on its own line */
    fputs("\n", path);

    /* Return with no errors */
    return LOG_ERROR_NONE;
}

int LogClose(FILE *path)
{
    /* Make sure there's a file to work with */
    if (path == NULL)
        return LOG_ERROR_NO_FILE;

    /* Put in the closing Time/Date */
    fputs("========== END LOG (", path);
    _logSetDate(path);
    fputs(") ==========\n", path);

    /* Close the file when finished */
    fclose(path);
}


#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* _CRSPTF_LOG_H */

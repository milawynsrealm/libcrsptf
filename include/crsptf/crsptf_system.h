/*
 * Project: Simple Cross-Platform Multimedia Library
 * License: MIT
 * Author: Lee Schroeder <spaceseel at gmail dot com>
 */
#ifndef _CRSPTF_SYSTEM_H
#define _CRSPTF_SYSTEM_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _CRSPTF_SHARED_H
#error crsptf_shared.h is needed to compile.
#endif

#if (CURRENT_OS == OSNAME_WINDOWS)
#include "_sys_w32.h"
#elif defined(IS_POSIX)
#include "_sys_psx.h"
#endif

int GetOsName(char *name)
{
#if (CURRENT_OS == OSNAME_WINDOWS)
    return GetOsName_win32(name);
#elif defined(IS_POSIX)
    return GetOsName_posix(name);
#elif (CURRENT_OS == OSNAME_ANDROID)
    strcpy(osName, "Android");
    return OSNAME_ANDROID;
#elif (CURRENT_OS == OSNAME_BEOS)
    strcpy(osName, "BeOS");
    return OSNAME_BEOS;
#elif defined(CURRENT_OS == OSTYPE_OS2)
    strcpy(osName, "OS/2");
    return OSNAME_OS2;
#else
    /* Shouldn't get here, but left here for debugging purposes */
    strcpy(name, "Unknown");
    return OSNAME_NONE;
#endif
}

int GetCompilerName(char *name)
{
#if defined(__BORLANDC__)
    sprintf(name, "Borland C++ %d", __BORLANDC__);
#elif defined(__clang__)
    /* Since Clang is compatible with GCC, check for it first
       before assuming it's the later */
    sprintf(name, "Clang %d.%d", __clang_major__, __clang_minor__);
#elif defined(__DMC__)
    sprintf(name, "Digital Mars %d", __DMC__);
#elif defined(__GNUC__)
    sprintf(name, "GNU GCC C/C++ %d.%d", __GNUC__, __GNUC_MINOR__);
#elif defined(_MSC_VER)
    sprintf(name, "Microsoft Visual C/C++ %d", _MSC_VER);
#else
    sprintf(name, "Unknown Compiler");
#endif
    return 0;
}

int GetProgramName(char *name)
{
#if (CURRENT_OS == OSNAME_WINDOWS)
    return ((GetModuleFileName(NULL, appName, MAX_PATH) == 0) ? 1 : 0);
#elif defined(IS_POSIX)
    return GetProgramName_posix(name);
#else
    return 0;
#endif
}

int GetArchName(void)
{
    return CURRENT_ARCH;
}

/* Determines what the user's name is */
int GetSystemUserName(char *name)
{
#if (CURRENT_OS == OSNAME_WINDOWS)
    DWORD uNameSize = 256;

    /* Grab the name of the user */
    return GetUserNameA(name, &uNameSize+1);
#else
    return ((getlogin_r(name, LOGIN_NAME_MAX) == 0) ? 0 : 1);
#endif /* _WIN32 */
}

char *GetSystemComputerName(void)
{
#if (CURRENT_OS == OSNAME_WINDOWS)
    DWORD compNameSize = 32767;
    char compName[compNameSize];

    /* Grab the name of the computer */
    if (GetComputerNameA(compName, &compNameSize))
        return NULL;
#else
    char compName[HOST_NAME_MAX];

    if (gethostname(compName, HOST_NAME_MAX) == -1)
        return NULL;
#endif
    return compName;
}

unsigned long GetSystemTotalMemory(void)
{
#define KB_CONV 1024
#if (CURRENT_OS == OSNAME_WINDOWS)
    MEMORYSTATUSEX sysMem;
    ULONGLONG sysMemCalc = 0;

    /* Get information about the system's memory */
    sysMem.dwLength = sizeof (sysMem);
    GlobalMemoryStatusEx (&sysMem);

    /* Calculate first to help prevent information loss */
    sysMemCalc = sysMem.ullTotalPhys / KB_CONV;

    /* Since it's now in Kilobytes, it doesn't really matter
     * if we cast into a smaller variable type */
    return (unsigned long)sysMemCalc;
#elif (CURRENT_OS == OSNAME_LINUX) || (CURRENT_OS == OSNAME_BSD)
    struct sysinfo sysMem;

    /* Get the system memory */
    if (sysinfo(&sysMem) == -1)
        return 0;

    return (sysMem.totalram / KB_CONV);
#else /* Misc */
    return 0;
#endif
}

int IsMinimumOS(int version)
{
#if (CURRENT_OS == OSNAME_WINDOWS)
    return IsMinimumOS_win32(version);
#else
    /* With exceptions, the base API pretty much stays the
     * same. */
    return 0;
#endif
}

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* _CRSPTF_SYSTEM_H */

/*
 * Project: Simple Cross-Platform Multimedia Library
 * License: MIT
 * Author: Lee Schroeder <spaceseel at gmail dot com>
 */

#include "../include/crsptf.h"
#if defined(_WIN32)
#include <winuser.h>
#else
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#endif

char *GetArchCode(void)
{
    /* Determine what kind of architecture is being used */
    switch(GetArchName())
    {
        case ARCH_86:
            return "ARCH_86";
        case ARCH_AMD64:
            return "ARCH_AMD64";
        case ARCH_ITANIUM:
            return "ARCH_ITANIUM";
        case ARCH_ARM:
            return "ARCH_ARM";
        case ARCH_ALPHA:
            return "ARCH_ALPHA";
        case ARCH_PPC:
            return "ARCH_PPC";
        default:
        {
            /* Shouldn't get here, but in case the user
             * is compiling on an unknown architecture */
            return "ARCH_NONE";
        }
    }
}

int AppMain(int argc, char *argv[])
{
    char tmpStr[256];

    /* Get all the strings compiled together */
    strcpy(tmpStr, "System Architecture: ");
    strcat(tmpStr, GetArchCode());

#if defined(_WIN32)
    MessageBoxA(NULL, tmpStr, "Cross Platform Library", MB_OK);
#else
    printf("%s\n", tmpStr);
#endif

    return 0;
}

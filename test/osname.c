/*
 * Project: Simple Cross-Platform Multimedia Library
 * License: MIT
 * Author: Lee Schroeder <spaceseel at gmail dot com>
 */

#include "../include/crsptf.h"
#if defined(_WIN32)
#include <winuser.h>
#else
#include <stdio.h>
#endif

char *GetOsCode(int code)
{
    switch(code)
    {
        case OSNAME_WINDOWS:
            return "OSNAME_WINDOWS";
        case OSNAME_LINUX:
            return "OSNAME_LINUX";
        case OSNAME_BSD:
            return "OSNAME_BSD";
        case OSNAME_MACOS:
            return "OSNAME_MACOS";
        case OSNAME_ANDROID:
            return "OSNAME_ANDROID";
        case OSNAME_BEOS:
            return "OSNAME_BEOS";
        case OSNAME_OS2:
            return "OSNAME_OS2";
        case OSNAME_HURD:
            return "OSNAME_HURD";
    }

    /* Return none if it goes here */
    return "OSNAME_NONE";
}

int AddSystem(void)
{
    int osVal = OSNAME_NONE;
    char tmpStr[256], osStr[128];

    /* Grabs the name of the Operating System */
    osVal = GetOsName(osStr);
    if (osStr == NULL)
        return 1;

    /* Stitches the OS name into a readable format */
    strcpy(tmpStr, "Operating System: ");
    strcat(tmpStr, osStr);
    strcat(tmpStr, " (");
    strcat(tmpStr, GetOsCode(osVal));
    strcat(tmpStr, ")");

#if defined(_WIN32)
    MessageBoxA(NULL, tmpStr, "Cross Platform Library", MB_OK);
#else
    printf("%s\n", tmpStr);
#endif

    return 0;
}

int AppMain(int argc, char *argv[])
{
    /* Grab the name of the operating system */
    if (AddSystem() != 0)
    {
        return 1;
    }

    return 0;
}

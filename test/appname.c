/*
 * Project: Simple Cross-Platform Multimedia Library
 * License: MIT
 * Author: Lee Schroeder <spaceseel at gmail dot com>
 */

#include "../include/crsptf.h"
#if defined(_WIN32)
#include <winuser.h>
#else
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#endif

int AppMain(int argc, char *argv[])
{
    char tmpStr[256], appName[128];

    /* Get all the strings compiled together */
    GetProgramName(appName);
    sprintf(tmpStr, "Application Name: %s", appName);

#if defined(_WIN32)
    MessageBoxA(NULL, tmpStr, "Cross Platform Library", MB_OK);
#else
    printf("%s\n", tmpStr);
#endif

    return 0;
}

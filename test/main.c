/*
 * Project: Simple Cross-Platform Multimedia Library
 * License: MIT
 * Author: Lee Schroeder <spaceseel at gmail dot com>
 */

#include "../include/crsptf.h"
#if defined(_WIN32)
#include <winuser.h>
#else
#include <stdio.h>
#endif

int AppMain(int argc, char *argv[])
{
#if defined(_WIN32)
    MessageBoxA(NULL, "Simple Application", "Cross Platform Library", MB_OK);
#else
    printf("Simple Application\n");
#endif
    return 0;
}

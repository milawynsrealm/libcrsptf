/*
 * Project: Simple Cross-Platform Multimedia Library
 * License: MIT
 * Author: Lee Schroeder <spaceseel at gmail dot com>
 */

#include "../include/crsptf.h"
#if defined(_WIN32)
#include <winuser.h>
#else
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#endif

int AppMain(int argc, char *argv[])
{
    char tmpStr[256], cmpName[128], useName[128];
    FILE *logFile = NULL;

    /* File to open */
    logFile = fopen("logtest.txt", "w");

    if (LogInit(logFile) != LOG_ERROR_NONE)
    {
        printf("ERROR: Unable to open 'logtest.txt'\n");
        return 0;
    }

    /* Get all the strings compiled together */
    sprintf(tmpStr, "Log Name: logtest.txt");

#if defined(_WIN32)
    MessageBoxA(NULL, tmpStr, "Cross Platform Library", MB_OK);
#else
    printf("%s\n", tmpStr);
#endif

    /* Print out the messages to the log */
    LogPrint(logFile, LOG_INFO, "This is the first message!");
    LogPrint(logFile, LOG_WARN, "There is a book on your desk!");
    LogPrint(logFile, LOG_ERROR, "The world is ending!");
    LogPrint(logFile, LOG_FATAL, "Run");

    /* Test out the ability to add variables */
    GetCompilerName(cmpName);
    LogPrint(logFile, LOG_INFO, "Compiler: %s", cmpName);

    GetSystemUserName(useName);
    LogPrint(logFile, LOG_INFO, "User Name: %s", useName);

    /* Close the file properly */
    LogClose(logFile);

    return 0;
}
